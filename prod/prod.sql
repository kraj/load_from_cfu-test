-- Transfer signature rights from CFU
Prompt Transfer signature rights from CFU to EDH
declare
  cursor new_sigs is
        (select ra.start_date start_date
               ,ra.end_date   end_date
               ,c.siriac_reference                entity_id
               ,ra.person_id
               ,decode(ra.role_type
                      ,'TECHNICAL',20000
                      ,'TECHNICAL2',10000
                      ,0
                      )+10*nvl(priority,0)        priority
               ,decode(ra.role_type
                      ,'OSVCVERIF','CONTRACTMGR'
                      ,'CNTTUSER','CNTTUSER'
                      ,'CNTTCOORD'
                      )                           role_type
         from cfu.roles@found_cfu     ra
             ,cfu.contracts@found_cfu c
         where 1=1
         and   exists
         (select null from cfper
          where pidcfper = ra.person_id
          and   atccfper = 'Y'
         )
         and  (ra.role_type in ('TECHNICAL','OSVCVERIF','CNTTUSER')
            or (ra.role_type = 'TECHNICAL2'
           and  not exists
                   (select null from cfu.roles@found_cfu
                    where person_id = ra.person_id
                    and   contract_id = ra.contract_id
                    and   role_type = 'TECHNICAL'
                    and   start_date <= trunc(sysdate)
                    and  (end_date >= trunc(sysdate) or end_date is null)
                   )
               )
              )
         and   ra.contract_id = cfu.cfucontrat.GetBaseContractId@found_cfu(c.id)
         and   ra.start_date <= trunc(sysdate)
         and  (ra.end_date >= trunc(sysdate) or ra.end_date is null)
         and   c.siriac_reference is not null
         union
         select ra.start_date
               ,ra.end_date
               ,numsamar
               ,ra.person_id
               ,10*nvl(ra.priority,0)
               ,'CONTRACTMGR'
         from cfu.roles@found_cfu     ra
             ,cfu.contracts@found_cfu c
             ,samar@wcern
         where 1=1
         and   exists
         (select null from cfper
          where pidcfper = ra.person_id
          and   atccfper = 'Y'
         )
         and   ra.role_type = 'OSVCVERIF'
         and   ra.contract_id = c.id
         and   ra.start_date <= trunc(sysdate)
         and  (ra.end_date >= trunc(sysdate) or ra.end_date is null)
         and   c.siriac_reference like 'C%'
         and   numsamar like 'Y%'
         and   substr(c.siriac_reference
                     ,2
                     ,decode(instr(c.siriac_reference,'/')
                            ,0,9999
                            ,instr(c.siriac_reference,'/')
                            )-2
                     )
              =substr(numsamar,2,decode(instr(numsamar,'/')
                                       ,0,9999
                                       ,instr(numsamar,'/')
                                       )-2
                     )
        ) -- Closes union
         minus
         select ras.start_date
               ,ras.end_date
               ,ras.entity1_id
               ,pidcfper
               ,ras.priority
               ,ras.role_type
         from role_assignments ras,cfper
         where 1=1
         and   ras.person_id = pidcfper
         and   ras.role_type in ('CONTRACTMGR','CNTTCOORD','CNTTUSER')
         and   trunc(sysdate) between ras.start_date and nvl(ras.end_date, sysdate)
         ;

  cursor deleted_sigs is
        select ras.role_assignment_id from role_assignments ras, cfper
        where trunc(sysdate) between ras.start_date and nvl(ras.end_date, sysdate)
        and   ras.role_type = 'CNTTCOORD'
        and   ras.person_id = pidcfper
        and   not exists
            (select null from cfu.roles@found_cfu ra
                              ,cfu.contracts@found_cfu c
              where 1=1
              and   ra.contract_id =
                    cfu.cfucontrat.GetBaseContractId@found_cfu(c.id)
              and   ra.person_id = pidcfper
              and   ra.start_date = ras.start_date
              and   start_date <= trunc(sysdate)
              and  (end_date >= trunc(sysdate) or end_date is null)
              and   ra.role_type in ('TECHNICAL','TECHNICAL2')
              and   c.siriac_reference = ras.entity1_id
            )
        union
        select ras.role_assignment_id from role_assignments ras, cfper
        where trunc(sysdate) between ras.start_date and nvl(ras.end_date, sysdate)
        and   ras.role_type = 'CONTRACTMGR'
        and   ras.entity1_id not like 'Y171A%'
        and   ras.entity1_id not like 'Y171B%'
        and   ras.person_id = pidcfper
        and   not exists
            (select null from cfu.roles@found_cfu ra,cfu.contracts@found_cfu c
              where 1=1
              and   ra.contract_id =
                    cfu.cfucontrat.GetBaseContractId@found_cfu(c.id)
              and   ra.person_id = pidcfper
              and   ra.start_date = ras.start_date
              and   start_date <= trunc(sysdate)
              and  (end_date >= trunc(sysdate) or end_date is null)
              and   ra.role_type = 'OSVCVERIF'
              and   c.siriac_reference = ras.entity1_id
              union
              select null from cfu.roles@found_cfu ra
                              ,cfu.contracts@found_cfu c
                              ,samar@wcern
              where 1=1
              and   ra.contract_id = c.id
              and   c.siriac_reference like 'C%'
              and   ra.person_id = pidcfper
              and   ra.start_date = ras.start_date
              and   start_date <= trunc(sysdate)
              and  (end_date >= trunc(sysdate) or end_date is null)
              and   ra.role_type = 'OSVCVERIF'
              and   numsamar = ras.entity1_id
              and   numsamar like 'Y%'
              and   substr(c.siriac_reference
                          ,2
                          ,decode(instr(c.siriac_reference,'/')
                                ,0,9999
                                ,instr(c.siriac_reference,'/')
                                )-2
                          )
                  = substr(numsamar,2,decode(instr(numsamar,'/')
                                            ,0,9999
                                            ,instr(numsamar,'/')
                                            )-2
                          )
            )
        ;
  v_sigid number;
  v_ident number;
  v_result number;
  v_date date;
begin

    for d in deleted_sigs loop
      -- UPDATE role_assignments
      -- SET end_date = sysdate
      -- WHERE role_assignment_id=d.role_assignment_id;
      dbms_output.put_line('DELETE ' || d.role_assignment_id);
    end loop;

    for r in new_sigs loop begin
    select pidcfper into v_ident
    from cfper
    where pidcfper = r.person_id
    and   atccfper = 'Y'
    and   idncfper is not null
    ;

    begin
      select role_assignment_id
      into v_sigid
      from role_assignments
      where 1=1
      and   person_id = v_ident
      and   role_type = r.role_type
      and   entity1_id = r.entity_id
      -- and   pfacfsra is null
      and   trunc(sysdate) between start_date and nvl(end_date, sysdate)
      ;
    exception
      when no_data_found then v_sigid := null;
    end;

    if v_sigid is null then
      begin
      select start_date
        into v_date
        from entities
      where 1 = 1
        and entity_type = upper(r.role_type)
        and entity_id = r.role_type
        and sysdate between start_date and nvl(end_date, sysdate);
      exception
        when no_data_found then
          dbms_output.put_line('Skipping: ' || r.entity_id);
          continue;
      end;                                          
      dbms_output.put_line('create( ' || r.entity_id || ' , ' || v_ident || ', ' || r.priority || ', ' || r.role_type || ', ' || r.start_date || ', ' || r.end_date);
      -- v_result := roles_api.create_role_assignment(
      --   p_role_type => r.role_type,
      --   p_person_id => v_ident,
      --   p_entity1_type => 'CONTRACT',
      --   p_priority => r.priority,
      --   p_entity1_id => r.entity_id,
      --   p_start_date => r.start_date,
      --   p_end_date => r.end_date
      -- );
    else
      -- UPDATE role_assignments
      -- SET priority = r.priority
      -- WHERE role_assignment_id = v_sigid;
      dbms_output.put_line('UPDATE(' || v_sigid || ' , ' || r.priority);
    end if;
  exception
    when no_data_found then -- Person without CERN id or not at CERN
      null;
  end; end loop;

  commit;
end;
