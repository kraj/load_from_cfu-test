# Testing

Ran the sql script against foundation@found_service_test and produced the output server_output.out

Step by step things I did

- Created a db link called `FOUND_CFU_TEST.WORLD` in `FOUND_SERVICE_TEST` to foundation@wcern_service_test.
`
CREATE DATABASE LINK "FOUND_CFU_TEST.WORLD"
   CONNECT TO "FOUNDATION"
   IDENTIFIED BY "YPpQm9w5_93XzGyD"
   USING 'wcern_service_test';
`
This db-link aleready exists in `FOUND_SERVICE_PROD` by the name of `FOUND_CFU.WORLD` and needs not to be created in prod.

- There is a `cfper` table in both foundation@found_service_prod and foundation@edh but they are different, since the upate_edh.sql script runs against foundation@edh, we need to carefully access the right table here.

- In FOUND_SERVICE_PROD db link to foundation@edh already exists by the name of `EDH.WORLD`. In FOUND_SERVICE_TEST db link to foundation@edh_service_test exists by the same name `EDH@WORLD`.

- Both edh and edh_service_test have the synonym SAMAR which which points to the `samar` table in db link `WCERN.WORLD`. In edh , `WCERN.WORLD` dblink links to `wcern_service_prod` and in `edh_service_test` it links to `wcern`. `found_service_prod` already has a DBlink to `wcern_service_prod` called by the same name `WCERN.WORLD` which could be used instead of the synonym or a similar synonym could be created for found_service_prod too. found_service_test has a dblink to wcern_service_test by the same name too which will be used for testing.

## Changes after suggestions

- We have to ultimately iterate over the new_sigs cursor and push the data rows into the role_assignments table. So the start_date and end_date being selected should have the same format as in the role_assignments table. In the role_assignments table the format for start_date and end_date is DD-MM-YY and the end date could also be (null). cfu.roles follows a simiar convention for dates, so all we have to do is to remove the formatting.

- Change all occurances of cfper@edh to cfper. We want to use the cfper in aisroles instead of the one in edh.

- cfsra_new@edh has to be replaced by role_assignments.


ROLE_ENTITY_TYPE_MAPPING
ROLE_TYPES
ROLE_FAMILY_MAPPING
